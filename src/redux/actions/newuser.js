import Toastr from 'toastr'
// import { restElement } from '@babel/types';

export const createUser = data => dispatch =>{
    callApi("users", {
        method:'post',
        body:JSON.stringify(data)
    }, res=>{
        dispatch({
            type:'CREATE_USERS',
            payload:res
        });
        Toastr.success("done")

    })
}

export function callApi(api, parms, onSuccess){
    // console.log('hhh',api)
    const url = `https://reqres.in/api/${api}`
    const vars = {
        ...parms , 
        headers:{
            'Content-Type':'application/json'
        }
        
    }
    fetch(url, vars)
    .then(res => res.status === 200 && res.json())
    .then(onSuccess)
    .catch(err => Toastr.error("error",err))
}
