import React, { Component } from 'react'
// import {createUser} from '../redux/actions/newuser'
import {connect } from 'react-redux'
import Userform from './reduxform'
import {updateUser} from '../redux/actions/updateuser'
 class Updateuser extends Component {
     handleSubmit=(values)=>{
        //  console.log("values",values)
        // console.log("update component props",this.props) 
       

        const {id } = this.props.match.params
        console.log(id)
        this.props.updateUser(values ,id  )
     }
     componentWillMount(){
     }
    render() {
        const values = this.props.user
        console.log("this values" , values)
        return (
        <Userform onSubmit={this.handleSubmit}
        initialValues={{
          email:values.email,
          firstName:values.first_name,
          lastName:values.last_name
        }}
        />    
        )
    }
}
const mapDispatchToProps = {
    updateUser
}
const mapStateToProps = state =>({
    user:state.users.user

})

export default connect(mapStateToProps , mapDispatchToProps)(Updateuser)

