import React, { Component } from 'react'
import { Link } from 'react-router-dom'
export default class Leftside extends Component {
    render() {
        return (
                <ul className="p-0">
                    <li><Link className="side_link p-3 text-dark d-block text-center" to="/fetchuser">Fetch users</Link> </li>
                    <li> <Link className="side_link p-3 text-dark d-block text-center" to="/newuser">new user</Link> </li>
                </ul>
            
        )
    }
}
