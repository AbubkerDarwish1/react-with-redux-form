import Toastr from 'toastr'
import { callApi } from './newuser'
export const updateUser = (data , id ) => dispatch =>{
  callApi("users/" + id , {
      method:'put',
      body:JSON.stringify(data)
  }, res=>{
    console.log("update ......")
      dispatch({
          type:'UPDATE_USER',
          payload:res
      });
      Toastr.success(" update done")

  })
}