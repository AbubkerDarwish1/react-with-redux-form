import {createStore , applyMiddleware } from 'redux'
import logger from "redux-logger"
import thunk from 'redux-thunk'
import combineReducers from './combinreducers'

const midlware = [thunk , logger]
const store = createStore( combineReducers ,applyMiddleware(...midlware) )

export default store