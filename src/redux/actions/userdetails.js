export const userDetails =(id)=> dispatch =>{
    fetch("https://reqres.in/api/users/" + id)
    .then(res => res.json())
    .then(user=>dispatch({
        type:"USER_DETAILS",
        payload:user
    }      
    ))
}