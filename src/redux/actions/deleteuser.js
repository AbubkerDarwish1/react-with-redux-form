import Toastr from 'toastr'

import {callApi} from './newuser'
 export const deleteUser = id => dispatch => {
  callApi("users/" + id , {
    method:'Delete'
    // body:JSON.stringify(id)
}, res=>{
    dispatch({
        type:'DELETE_USER',
        payload:res
    });
    Toastr.success("done")

})
    // console.log("delete user")
 
}


// export const deleteUser = (id) => dispatch => {
//   console.log("delete user")
//   return fetch("https://reqres.in/api/users/" + id, {
//     method: 'delete'
//   })
//   .then(res => res.json())
//   .then(
//     data=>{
//       dispatch({  
//         type:'DELETE_USERS',
//         payload:data
//     })
//     Toastr.success("delete done")
    

    
//     }
//   ).catch(err => Toastr.error("error"))
// }