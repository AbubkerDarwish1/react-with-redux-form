import {combineReducers} from 'redux'
import {users} from './../producer/users'
import { reducer as formReducer } from 'redux-form';
export default  combineReducers({
users,
form:formReducer
})