
const initState={
    users: [],
    user: {},
}
export const users = (state = initState , action) => {

    switch (action.type) {
        case 'FETCH_USERS':
            return { ...state ,  users: action.payload.data }    
        case'USER_DETAILS':
            return{ ...state, user:action.payload.data }
        case 'CREATE_USERS':
            return{ ...state }
        case 'DELETE_USER':
            return{ ...state }
        case 'UPDATE_USER':
            return{ ...state }
        default:
            return state
    }

}
