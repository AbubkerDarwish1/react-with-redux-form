import React from 'react';
import { Field, reduxForm } from 'redux-form';

const Userform = ({ handleSubmit, reset}) => (

    <div>
      <div>
        <label>First Name</label>
        <div>
          <Field
            name="firstName"
            component="input"
            type="text"
            placeholder="First Name"
            
          />            
        </div>
      </div>
      <div>
        <label>Last Name</label>
        <div>
          <Field
            name="lastName"
            component="input"
            type="text"
            placeholder="Last Name"
          />
        </div>
      </div>
      <div>
        <label>Email</label>
        <div>
          <Field
            name="email"
            component="input"
            type="email"
            placeholder="Email"
          />
        </div>
      </div>
      <div>
        <button type="submit" onClick={handleSubmit}>Submit</button>
        <button type="button" onClick={reset}>
          Clear Values
        </button>
      </div>
    </div>
);

export default reduxForm({
  form: 'createUser',
  enableReinitialize : true
})(Userform);
