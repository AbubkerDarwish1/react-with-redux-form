
export const fechusers = () => dispatch =>{
    fetch("https://reqres.in/api/users?page=2")
    .then(res => res.json())
    .then( users=>
        dispatch({
            type:'FETCH_USERS',
            payload:users
        })
    )
}