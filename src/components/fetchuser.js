import React, { Component } from 'react'
import {connect } from 'react-redux'
import {fechusers}  from './../redux/actions/fetchusers'
import {deleteUser} from '../redux/actions/deleteuser'
import {Link} from 'react-router-dom'
class Fetchuser extends Component {

    componentWillMount(){
        this.props.dispatch(fechusers())
        console.log("willamont")
    }
  
    render() {
    
        return (
            <div className="contanier">
                <table className="table table-hover text-center">
                    <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Go details</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.users.map(user=>{
                        return(
                            <tr key={user.id}>
                                <td>{user.first_name}</td>
                                <td>{user.last_name}</td>
                                <td>{user.email}</td>
                                <td><Link className="btn btn-success btn-sm " to={`/fetchuser/user/${user.id}`}>details</Link></td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        )
    }
}
const mapStateToProps = state =>({
    users:state.users.users

})
// const mapDispatchToProps = state =>({
// })
export default connect(mapStateToProps)(Fetchuser)
