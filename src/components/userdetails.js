import React, { Component } from 'react'
import {connect } from 'react-redux'
import {userDetails}  from '../redux/actions/userdetails'
import {deleteUser} from '../redux/actions/deleteuser'
// import {updateUser} from '../redux/actions/updateuser'
// import Userform from '../components/reduxform'
// import {Link} from 'react-router-dom'
// import { timeout } from 'q';
 class Userdetails extends Component {
     
     componentWillMount(){
        //  console.log("rrrrrrrrr" , this.props)
        const {id} =this.props.match.params
        this.props.dispatch(userDetails(id))
     }
      onDelete = () => {
        //   console.log("done")
        const {id} =this.props.match.params
        this.props.dispatch(deleteUser(id))
        setTimeout(()=>this.props.history.push(`/fetchuser`),2000)
        // this.props.dispatch(fechusers())
    }
    onUpdate = ()=>{
        const {id} =this.props.match.params
        // console.log(id)
        // const {data} = this.props.user
        // this.props.dispatch(updateUser( data , id))
        // console.log("rrrrrrrrrrr" , this.props)
        // <Userform />
        this.props.history.replace("/Updateuser/user/"+id )
        
    }
    render() {
        
        const user =this.props.user
        // console.log("kjreihrjoeir",user)
        return (
            <div>
                <img src={user.avatar} />
                <span>{user.first_name} {user.last_name}</span>
                <button className="btn btn-danger btn-sm" onClick={this.onDelete}  >delete</button> 
                <button className="btn btn-primary btn-sm" onClick={this.onUpdate}>Edit</button>
            </div>
        )
    }
}

const mapStateToProps = state =>({
    user:state.users.user

})
// const mapDispatchToProps = state =>({
// })
export default connect(mapStateToProps)(Userdetails)
