import React from 'react'
import './App.css'
import {BrowserRouter as Router , Route } from 'react-router-dom'
import Leftside from './components/header/leftside'
import Fetchuser from './components/fetchuser'
import Createuser from './components/newuser'
import Userdetails from './components/userdetails'
import {Provider} from 'react-redux'
import store from './redux/mange/store'
import Updateuser from './components/updateuser'
import 'toastr/build/toastr.min.css'
function App() {
  return (
    <Provider store={store}>
      <Router>
      <div className="container-fluid App">
        <div className="row">
          <div className="col-3">
            <Leftside />  
          </div>
          <div className="col-9">
           
            <Route path="/fetchuser" exact component={Fetchuser} />
            <Route path="/newuser" exact component={Createuser} />
            <Route path="/fetchuser/user/:id" exact component={Userdetails}/>
            <Route path="/Updateuser/user/:id" exact component={Updateuser}/>
          </div>
        </div>  
      </div>
      </Router>
    </Provider>
  );
}

export default App;
