import React, { Component } from 'react'
import {createUser} from '../redux/actions/newuser'
import {connect } from 'react-redux'
import Userform from './reduxform'
// import {createUser} from '../redux/actions/newuser'
 class UserForm extends Component {
     handleSubmit=values=>{
        //  console.log("values",values)
        this.props.createUser(values)
     }
    render() {
        console.log(this.props)
        return (
        <Userform onSubmit={this.handleSubmit}/>    
        )
    }
}
const mapDispatchToProps = {
    createUser
}
const mapStateToProps = state =>({
    user:state.users.user

})

export default connect(mapStateToProps , mapDispatchToProps)(UserForm)

